var express = require('express');
var logger = require('morgan');
var fs = require('fs');
var app = express();
const request = require('request');

app.use(logger('dev'));
app.use(express.static('public'))

app.get('/', function(req, res){
  // var text = "env = {"+
  // "  BEER_ORDER_SERVER_SERVICE_SERVICE_HOST: '"+process.env.BEER_ORDER_SERVER_SERVICE_SERVICE_HOST+"',\n"+
  // "  BEER_ORDER_SERVER_SERVICE_SERVICE_PORT: '"+process.env.BEER_ORDER_SERVER_SERVICE_SERVICE_PORT+"',\n"+
  // "  BEER_STOCK_SERVER_SERVICE_SERVICE_HOST: '"+process.env.BEER_STOCK_SERVER_SERVICE_SERVICE_HOST+"',\n"+
  // "  BEER_STOCK_SERVER_SERVICE_SERVICE_PORT: '"+process.env.BEER_STOCK_SERVER_SERVICE_SERVICE_PORT+"',\n"+
  // "  DRONE_APP_SERVICE_HOST: '"+process.env.DRONE_APP_SERVICE_HOST+"',\n"+
  // "  DRONE_APP_SERVICE_PORT: '"+process.env.DRONE_APP_SERVICE_PORT+"',\n"+
  // "  DELIVERY_APP_SERVICE_HOST: '"+process.env.DELIVERY_APP_SERVICE_HOST+"',\n"+
  // "  DELIVERY_APP_SERVICE_PORT: '"+process.env.DELIVERY_APP_SERVICE_PORT+"',\n"+
  // "}";

  // fs.writeFile("./public/javascripts/env.js", text, function(err) {
  //   if(err) {
  //       return console.log(err);
  //   }

    console.log("The file was saved!");
    res.sendFile(__dirname + '/public/main_page.html');
  //}); 

});

app.get('/getDrones', function(req, response){
  request('http://'+ process.env.DRONE_APP_SERVICE_HOST + ':' + process.env.DRONE_APP_SERVICE_PORT + '/getDrones', { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    response.send(res.body);
  });
})

app.get('/addDrone', function(req, response){
  request('http://'+ process.env.DRONE_APP_SERVICE_HOST + ':' + process.env.DRONE_APP_SERVICE_PORT + '/addDrone', { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    response.send(res.body);
  });
})

app.get('/orders', function(req, response){
  request('http://'+ process.env.BEER_ORDER_SERVER_SERVICE_SERVICE_HOST + ':' + process.env.BEER_ORDER_SERVER_SERVICE_SERVICE_PORT + '/orders', { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    response.send(res.body);
  });
})

app.get('/beers', function(req, response){
  request('http://'+ process.env.BEER_STOCK_SERVER_SERVICE_SERVICE_HOST + ':' + process.env.BEER_STOCK_SERVER_SERVICE_SERVICE_PORT + '/beers', { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    response.send(res.body);
  });
})

app.get('/getDeliveries', function(req, response){
  request('http://'+ process.env.DELIVERY_APP_SERVICE_HOST + ':' + process.env.DELIVERY_APP_SERVICE_PORT + '/getDeliveries', { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    response.send(res.body);
  });
})

app.get('/deleteDeliveries', function(req, response){
  request('http://'+ process.env.DELIVERY_APP_SERVICE_HOST + ':' + process.env.DELIVERY_APP_SERVICE_PORT + '/deleteDeliveries', { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    response.send(res.body);
  });
})

app.get('/placeOrder', function(req, response){
  request('http://'+ process.env.BEER_ORDER_SERVER_SERVICE_SERVICE_HOST + ':' + process.env.BEER_ORDER_SERVER_SERVICE_SERVICE_PORT + '/placeOrder?amount=' + parseInt(req.query.amount) + "&type=" + req.query.type, { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    response.send(res.body);
  });
})

app.get('/addBeer', function(req, response){
  request('http://'+ process.env.BEER_STOCK_SERVER_SERVICE_SERVICE_HOST + ':' + process.env.BEER_STOCK_SERVER_SERVICE_SERVICE_PORT + '/addBeer?beeramount=' + parseInt(req.query.beeramount) + "&beertype=" + req.query.beertype, { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    response.send(res.body);
  });
})

app.get('/deleteAllOrders', function(req, response){
  request('http://'+ process.env.BEER_ORDER_SERVER_SERVICE_SERVICE_HOST + ':' + process.env.BEER_ORDER_SERVER_SERVICE_SERVICE_PORT + '/deleteAllOrders', { json: true }, (err, res, body) => {
    if (err) { return console.log(err); }
    response.send(res.body);
  });
})


module.exports = app;
