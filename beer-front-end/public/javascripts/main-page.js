﻿// Endpoints should be defined based on host and port of services from env variables.

/*
var ordersServiceHost = "http://192.168.99.100"; 
var ordersServicePort = "31416"; 

var beerStockServiceHost = "http://192.168.99.100"; 
var beerStockServicePort = "31601"; 
*/

// var droneAppServiceHost =  env.DRONE_APP_SERVICE_HOST;
// var droneAppServicePort = env.DRONE_APP_SERVICE_PORT;

// var deliveriesServiceHost = env.DELIVERY_APP_SERVICE_HOST;
// var deliveriesServicePort = env.DELIVERY_APP_SERVICE_PORT;


var ordersEndpoint = /*ordersServiceHost + ":" + ordersServicePort +*/  "/orders";
var beersEndpoint = /*beerStockServiceHost + ":" + beerStockServicePort +*/  "/beers";
var dronesEndpoint = /*droneAppServiceHost + ":" + droneAppServicePort +*/  "/getDrones";
var deliveriesEndpoint = /*deliveriesServiceHost + ":" + deliveriesServicePort + */ "/getDeliveries";
var deleteDeliveriesEndpoint = /*deliveriesServiceHost + ":" + deliveriesServicePort +*/  "/deleteDeliveries";
var addDroneEndpoint = /*droneAppServiceHost + ":" + droneAppServicePort +*/ "/addDrone";
var placeOrderEndpoint = /*ordersServiceHost + ":" + ordersServicePort +*/ "/placeOrder";
var addBeerEndpoint = /*beerStockServiceHost + ":" + beerStockServicePort +*/ "/addBeer";
var deleteAllOrdersEndpoint = /*ordersServiceHost + ":" + ordersServicePort +*/ "/deleteAllOrders";


var tableStyle = "border: 1px solid black;text-align: center; width:90%;margin:auto;";
var tableProperties = "border=\"1\" cellpadding=\"2\" cellspacing=\"0\" ";

function fetchOrders() {
    $.get(ordersEndpoint, function(data, status){
            var htmlOutput = "";
            
            htmlOutput += "<table style=\"" + tableStyle + "\" "+ tableProperties + ">";
            htmlOutput += "<tr>" + "<td> Id </td>" + "<td> Status </td>" + "<td> BeerType </td>" + "<td> Amount </td>" + "</tr>";
           

            var orders = JSON.parse(JSON.stringify(data));
            for (i = 0; i < orders.length; i++) {
                htmlOutput += "<tr>" + "<td>" + orders[i].id + "</td>" + "<td>" + orders[i].status + "</td>" + "<td>" + orders[i].beerType + "</td>" + "<td>" + orders[i].amount + "</td>" + "</tr>";
            }
            htmlOutput += "</table>";
            
            $("#orders").html(htmlOutput);
        });
}

function fetchBeers() {
    $.get(beersEndpoint, function(data, status){
            var htmlOutput = "";
            
            htmlOutput += "<table style=\"" + tableStyle + "\" "+ tableProperties + ">";
            htmlOutput += "<tr>" + "<td> Id </td>" + "<td> Type </td>" + "<td> In Stock </td>" + "</tr>";
           

            var beers = JSON.parse(JSON.stringify(data));
            for (i = 0; i < beers.length; i++) {
                htmlOutput += "<tr>" + "<td>" + beers[i].id + "</td>" + "<td>" + beers[i].type + "</td>" + "<td>" + beers[i].in_stock + "</td>" + "</tr>";
            }
            htmlOutput += "</table>";
            
            $("#beers").html(htmlOutput);
        });
}

function fetchDrones() {
    $.get(dronesEndpoint, function(data, status){
            var htmlOutput = "";
            
            htmlOutput += "<table style=\"" + tableStyle + "\" "+ tableProperties + ">";
            htmlOutput += "<tr>" + "<td> Id </td>" + "<td> Is Free </td>" + "<td> Assigned delivery </td>" + "</tr>";
           
            var drones = data;
            for (i = 0; i < drones.length; i++) {
                htmlOutput += "<tr>" + "<td>" + drones[i].droneID + "</td>" + "<td>" + drones[i].free + "</td>" + "<td>" + drones[i].deliveryID + "</td>" + "</tr>";
            }
            htmlOutput += "</table>";
            
            $("#drones").html(htmlOutput);
        });
}

function addDrone() {
    $.get(addDroneEndpoint, function(data, status){
        fetchDrones();
    });
}

function fetchDeliveries() {
    $.get(deliveriesEndpoint, function(data, status){
        var htmlOutput = "";
        
        htmlOutput += "<table style=\"" + tableStyle + "\" "+ tableProperties + ">";
        htmlOutput += "<tr>" + "<td> Id </td>" + "<td> DroneID </td>" + "<td> OrderID </td>" + "<td> Status </td>" + "</tr>";
        
        var deliveries = data;
        for (i = 0; i < deliveries.length; i++) {
            htmlOutput += "<tr>" + "<td>" + deliveries[i].deliveryID + "</td>" + "<td>" + deliveries[i].droneID + "</td>" + "<td>" + deliveries[i].orderID + "</td>" + "<td>" + deliveries[i].status + "</td>" + "</tr>";
        }
        htmlOutput += "</table>";
            
        $("#deliveries").html(htmlOutput);
    });
}

function deleteDeliveries() {
    $.get(deleteDeliveriesEndpoint, function(data, status){
        fetchDeliveries();
    });
}

function addBeer() {
    var parameters = "?beeramount=" + $("#addBeerAmount").val() + "&beertype=" + $("#addBeerType").val();
    $.get(addBeerEndpoint + parameters, function(data, status){

    });
}

function placeOrder() {
    var parameters = "?amount=" + $("#placeOrderAmount").val() + "&type=" + $("#placeOrderType").val();
    $.get(placeOrderEndpoint + parameters, function(data, status){
            $("#placeOrderResponse").html(data);
    });
}

function deleteAllOrders() {
    $.get(deleteAllOrdersEndpoint, function(data, status){
    });
}


function refreshAll() {
    fetchBeers();
    fetchDeliveries();
    fetchDrones();
    fetchOrders();
    console.log("Fetched all entities (beers,deliveries,drones,orders).");
}