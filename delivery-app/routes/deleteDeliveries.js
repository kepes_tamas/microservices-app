var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

/* Delete deliveries */
router.get('/', function(req, response, next) {
  var mongoUrl = "mongodb://"+ process.env.MONGODB_SERVICE_HOST + ":" + process.env.MONGODB_SERVICE_PORT + "/";
  
  MongoClient.connect(mongoUrl, function(err, db) {
    if (err) throw err;
    var dbo = db.db("spring-boot-jpa-mongo-exemple");
    dbo.collection("deliveries").remove({status: "finished"}, function(err, dataFromDB) {
      if (err) throw err;
      response.sendStatus(200);
      db.close();
    });
  });
});

module.exports = router;