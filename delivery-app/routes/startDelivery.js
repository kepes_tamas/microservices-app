var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;
const request = require('request');

/* Start delivery */
router.get('/', function(req, response, next) {
  var mongoUrl = "mongodb://"+ process.env.MONGODB_SERVICE_HOST + ":" + process.env.MONGODB_SERVICE_PORT + "/";
  var droneAppUrl = 'http://' + process.env.DRONE_APP_SERVICE_HOST;
  var droneAppPort = process.env.DRONE_APP_SERVICE_PORT;
  var orderServiceUrl = 'http://' + process.env.BEER_ORDER_SERVER_SERVICE_SERVICE_HOST;
  var orderServicePort = process.env.BEER_ORDER_SERVER_SERVICE_SERVICE_PORT;

  var orderID = req.query.orderID;
  console.log("Got order with ID : " + orderID);
  
  MongoClient.connect(mongoUrl, function(err, db) {
    if (err) throw err;
    var dbo = db.db("spring-boot-jpa-mongo-exemple");

    //generate new delivery
    var mysort = { deliveryID: -1 };
    dbo.collection("deliveries").find().sort(mysort).toArray(function(err, result) {
      if (err) throw err;
      if (result[0] == undefined) deliveryID = 1;
      else deliveryID = result[0].deliveryID + 1;

      //drone request
      request(droneAppUrl + ':' + droneAppPort + '/acquireDrone?deliveryID=' + deliveryID, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
        if (body == null) { return console.log("Failed to acquire drone"); }
        
        var droneID = body.droneID;
        console.log("Acquired drone with ID : " + droneID);

          dbo.collection("deliveries").insertOne({deliveryID: deliveryID, droneID: droneID, orderID: orderID, status: "In progress"}, function(err, dataFromDB) {
            if (err) throw err;
            console.log("Inserted one delivery");

            //simulating delivey
            setTimeout(() => {
              console.log("Delivery with ID : " + deliveryID + " was successful");

              //drone freeup
              request(droneAppUrl + ':' + droneAppPort + '/freeUpDrone?droneID=' + droneID, { json: false }, (err, res, body) => {
                if (err) { return console.log(err); }
                if (res.statusCode == 200) {
                  console.log("Drone with ID : " + droneID + " was successfully freed-up");
                }
                
                //successful delivery send
                request(orderServiceUrl + ':' + orderServicePort + '/markSuccessfulDelivery?orderId=' + orderID, { json: false }, (err, res, body) => {
                  if (err) { return console.log(err); }
                  if (res.statusCode == 200) {
                    console.log("Successful delivery");
                  }
                  
                  //set delivery status
                  var query = { deliveryID: deliveryID };
                  var newvalues = { $set: {deliveryID: deliveryID, droneID: droneID, orderID: orderID, status: "finished" } };
                  dbo.collection("deliveries").updateOne(query, newvalues, function(err, res) {
                    if (err) throw err;
                    console.log("One delivery updated");
                    db.close();
                  });
                });        
              });
            }, 4000);
          });
        response.sendStatus(200);
      });
    });  
  });
});

module.exports = router;
