var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

/* GET deliveries listing */
router.get('/', function(req, response, next) {
  var mongoUrl = "mongodb://"+ process.env.MONGODB_SERVICE_HOST + ":" + process.env.MONGODB_SERVICE_PORT + "/";
  
  MongoClient.connect(mongoUrl, function(err, db) {
    if (err) throw err;
    var dbo = db.db("spring-boot-jpa-mongo-exemple");
    dbo.collection("deliveries").find({}).toArray(function(err, dataFromDB) {
      if (err) throw err;
      var result = [];
      for (var id in dataFromDB) {
        result.push(dataFromDB[id]);
      }
      response.json(result);
      db.close();
    });
  });
});

module.exports = router;
