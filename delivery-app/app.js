var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var getDeliveries = require('./routes/getDeliveries');
var startDelivery = require('./routes/startDelivery');
var deleteDeliveries = require('./routes/deleteDeliveries');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())

app.use('/getDeliveries', getDeliveries);
app.use('/startDelivery', startDelivery);
app.use('/deleteDeliveries', deleteDeliveries);

module.exports = app;
