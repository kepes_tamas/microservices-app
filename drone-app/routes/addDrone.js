var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

/* Add a drone */
router.get('/', function(req, response, next) {
  var mongoUrl = "mongodb://"+ process.env.MONGODB_SERVICE_HOST + ":" + process.env.MONGODB_SERVICE_PORT + "/";
  
  MongoClient.connect(mongoUrl, function(err, db) {
        if (err) throw err;
        var dbo = db.db("spring-boot-jpa-mongo-exemple");
        var mysort = { droneID: -1 };
        dbo.collection("drones").find().sort(mysort).toArray(function(err, result) {
            if (err) throw err;
            if (result[0] == undefined) droneID = 1;
            else droneID = result[0].droneID + 1;

            dbo.collection("drones").insertOne({droneID: droneID, free: true, deliveryID: null}, function(err, dataFromDB) {
                if (err) throw err;
                console.log("Inserted one drone");
                db.close();
                response.sendStatus(200);
            });
        });
  });
});

module.exports = router;
