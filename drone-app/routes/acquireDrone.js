var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;


/* Acquires a Drone if there is a free one, else sends null */
router.get('/', function(req, response, next) {
  var mongoUrl = "mongodb://"+ process.env.MONGODB_SERVICE_HOST + ":" + process.env.MONGODB_SERVICE_PORT + "/";
  var deliveryID = req.query.deliveryID;


  MongoClient.connect(mongoUrl, function(err, db) {
    if (err) throw err;
    var dbo = db.db("spring-boot-jpa-mongo-exemple"); //database name

    //check for free drone with db
    var query = { free : true };
    dbo.collection("drones").find(query).toArray(function(err, result) {
      if (err) throw err;
      if (result[0] == undefined) freeDrone = false;
      else {
        var freeDrone = true;
        var freeDroneID = result[0].droneID;
      }
      if (freeDrone) {
        //set free drone to in-use and set deliveryID
        var updateQuery = { droneID: result[0].droneID };
        var newvalues = { $set: {droneID: result[0].droneID, free: false, deliveryID: deliveryID } };
        dbo.collection("drones").updateOne(updateQuery, newvalues, function(err, res) {
          if (err) throw err;
          console.log("1 drone acquired");
          console.log("Acquired drone for delivery with ID : " + deliveryID);
          db.close();
          response.json({droneID : freeDroneID});
        });
      } else {
        console.log("Failed to acquire drone for delivery with ID : " + deliveryID);
        db.close();
        response.json(null);
      }
    });
  }); 

  



});

module.exports = router;
