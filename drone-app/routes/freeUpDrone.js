var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

/*Free up drone */
router.get('/', function(req, response, next) {
  var mongoUrl = "mongodb://"+ process.env.MONGODB_SERVICE_HOST + ":" + process.env.MONGODB_SERVICE_PORT + "/";
  var droneID = req.query.droneID;

  MongoClient.connect(mongoUrl, function(err, db) {
    if (err) throw err;
    var dbo = db.db("spring-boot-jpa-mongo-exemple"); //database name

    //check for not free drone with db
    var query = { free : false };
    dbo.collection("drones").find(query).toArray(function(err, result) {
      if (err) throw err;
      if (result == []) freeDrone = false;
      else {
        var freeDrone = true;
        var freeDroneID = result[0].droneID;
      }
      if (freeDrone) {
        for (var id in result) {
          if (result[id].droneID == droneID)
            var freeUp = id;
        }
        var updateQuery = { droneID: result[freeUp].droneID };
        var newvalues = { $set: {droneID: result[freeUp].droneID, free: true, deliveryID: null } };
        dbo.collection("drones").updateOne(updateQuery, newvalues, function(err, res) {
          if (err) throw err;
          console.log("1 drone freed up");
          console.log("Freed up " + droneID);
          db.close();
          response.sendStatus(200);
        });
      } else {
        db.close();
        response.json(null);
      }
    });
  }); 
});


module.exports = router;
