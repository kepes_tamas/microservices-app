var express = require('express');
var router = express.Router();
var MongoClient = require('mongodb').MongoClient;

/* GET drones listing */
router.get('/', function(req, res, next) {

  var mongoUrl = "mongodb://"+ process.env.MONGODB_SERVICE_HOST + ":" + process.env.MONGODB_SERVICE_PORT + "/";
  
  MongoClient.connect(mongoUrl, function(err, db) {
    if (err) throw err;
    var dbo = db.db("spring-boot-jpa-mongo-exemple"); //db name
    dbo.collection("drones").find({}).toArray(function(err, dataFromDB) {
      if (err) throw err;
      //var dataFromDB = [{droneID : 1, status: 'taken'}, {droneID : 2, status: 'free'}, {droneID : 3, status: 'taken'}];
      var result = [];
      for (var id in dataFromDB) {
        result.push(dataFromDB[id]);
      }
      res.json(result);
      db.close();
    });
  });
});

module.exports = router;
