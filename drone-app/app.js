var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var acquireDrone = require('./routes/acquireDrone');
var freeUpDrone = require('./routes/freeUpDrone');
var getDrones = require('./routes/getDrones');
var addDrone = require('./routes/addDrone');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser())

app.use('/acquireDrone', acquireDrone);
app.use('/freeUpDrone', freeUpDrone);
app.use('/getDrones', getDrones);
app.use('/addDrone', addDrone);

module.exports = app;
